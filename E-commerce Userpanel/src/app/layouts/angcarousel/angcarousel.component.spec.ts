import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngcarouselComponent } from './angcarousel.component';

describe('AngcarouselComponent', () => {
  let component: AngcarouselComponent;
  let fixture: ComponentFixture<AngcarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngcarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngcarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
