import { Component, OnInit } from '@angular/core';
import { UserService} from '../../services/user.service';
import { User } from '../../models/user2';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers:[UserService]
})
export class UserComponent implements OnInit {
  users:User[];
  user:User;
  firstName:string;
  lastName:string;
  userName:string;
  password:string;

  constructor(private userService : UserService) { }
  
  ngOnInit() {
    this.loadAllUsers();
  }
  loadAllUsers() {
    this.userService.getUsers()
        .pipe(first())
        .subscribe(users => this.users = users);
  }

  addDetails(){
    const User = {
      firstName : this.firstName,
      lastName : this.lastName,
      userName: this.userName,
      password: this.password
    }
    this.userService.register(User)
    .subscribe(users => {
      this.users.push(User);
      });
      this.loadAllUsers();
    }

  deleteDetails(_id: number) {
    this.userService.deleteUser(_id)
        .pipe(first())
        .subscribe(() => this.loadAllUsers());
  }
}