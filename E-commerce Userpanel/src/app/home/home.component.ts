import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '.././services/authentication.service';
import { Router } from '@angular/router';
import { AsyncPipe } from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

currentUser: any;
  loggedIn
  constructor(private router: Router,
    private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() { 
    this.loggedIn = JSON.parse(localStorage.getItem('loggedIn'))
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);

  }
}
