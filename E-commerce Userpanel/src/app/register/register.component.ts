import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { MustMatch } from '../shared/must-match.validator';
import { HttpClient } from '@angular/common/http';
import { ApiService} from '../services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[ApiService]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  // uemail = new FormControl('', [Validators.required, Validators.email]);
  // ucemail = new FormControl('', [Validators.required, Validators.email]);

  constructor(
    private router: Router, 
    private formBuilder: FormBuilder, 
    private http: HttpClient,
    private apiService: ApiService) { }
  first_name:string;
  last_name:string;
  email: string;
  email2:string;
  phone:string;
  password: string;
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      email2: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required,Validators.minLength(10)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  } 
//   {validator: MustMatch('email', 'email2')}
  
  );
  }
  get f() { return this.registerForm.controls; }

  public hasError = (controlName: string, errorName: string) =>{
    return this.registerForm.controls[controlName].hasError(errorName);
  }
  
  // getEmailErrorMessage() {
  //   return this.uemail.hasError('required') ? 'You must enter a value' :
  //       this.uemail.hasError('email') ? 'Not a valid email' :
  //           '';
  // }

  // getCemailErrorMessage() {
  //   return this.ucemail.hasError('required') ? 'You must enter a value' :
  //       this.ucemail.hasError('email') ? 'Not a valid email' :
  //           '';
  // }
  
  onSubmit() {
    debugger;
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }
      let dataToSend = {
        first_name:this.registerForm.value.first_name,
        last_name:this.registerForm.value.last_name,
        email : this.registerForm.value.email,
        email2 : this.registerForm.value.email2,
        phone : this.registerForm.value.phone,
        password : this.registerForm.value.password,

      };
      if(this.registerForm.valid){
        this.apiService.register(dataToSend).subscribe((response)=>{
          console.log('repsonse ',response);
          this.router.navigate(["login"]);
        });
        }

      // display form values on success
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  onReset() {
      this.submitted = false;
      this.registerForm.reset();
  }
}