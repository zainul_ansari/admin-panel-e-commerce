import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from './services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
) {}

canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
  let url: string = state.url;

  return this.checkLogin(url);
}
checkLogin(url: string): boolean {
  if (this.authenticationService.login) { return true; }

  // Store the attempted URL for redirecting
  this.authenticationService.baseUrl = url;

  // Navigate to the login page with extras
  this.router.navigate(['/login']);
  return false;
}
}

