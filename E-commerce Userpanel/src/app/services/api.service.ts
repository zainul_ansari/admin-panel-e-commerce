import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl = "http://localhost:8000/api/accounts";
  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  })

  
  selectedUser
  constructor(private http : HttpClient) { }
  
  getAllUsers() : Observable<any>{
    return this.http.get(this.baseUrl + '/users_lists',
    {headers : this.httpHeaders});
  }

  getById(id) : Observable<any> {
    return this.http.get(this.baseUrl + '/users_details/'+ id);
  }

  register(user) : Observable<any> {
    return this.http.post(this.baseUrl + '/register/',user);
  }
  
  // update(id, User:{username, email, phone}) :Observable<any>{
  //   return this.http.put(this.baseUrl + '/edit/'+ id, User,
  //   {headers : this.httpHeaders});
  // }

  update(user) {  
    console.log(user);  
    const params = new HttpParams().set('id', user.id);  
    const headers = new HttpHeaders().set('content-type', 'application/json');  
    var body = {  
      username: user.username, email: user.email, phone: user.phone, password: user.password, id: user.id  
      }  
    return this.http.put<User>(this.baseUrl + '/edit/' + user.id, body, { headers, params })  
  
  }  

  delete(user) : Observable<any>{
    return this.http.delete(this.baseUrl + '/delete/' + user);
  }

}

