import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { User } from '../models/user2';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
    constructor(private http: HttpClient) { }

    getUsers() :Observable<User[]>{
        return this.http.get<User[]>(`http://localhost:3000/api/users`);
    }
    
//   getContacts() :Observable<Contact>{
//     return this.http.get<Contact>('http://localhost:3000/api/contacts')
//   }
    getById(_id: number) {
        return this.http.get(`/users/` + _id);
    }

    // register(User) {
    //     return this.http.post(`http://localhost:3000/api/user`, User);
    // }
    
    register(users:User){
    var headers = new  HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/user', users,{headers:headers})
    }

    update(user: User) {
        return this.http.put(`/users/` + user._id, user);
    }

    deleteUser(_id: number) {
        return this.http.delete(`/users/` + _id);
    }
}