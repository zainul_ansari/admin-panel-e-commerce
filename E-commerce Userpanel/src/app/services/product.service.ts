import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { HttpEvent, HttpErrorResponse, HttpEventType } from  '@angular/common/http';
import { map } from  'rxjs/operators';
  
@Injectable({
  providedIn: 'root'
})
export class ProductService {  
    baseUrl = "http://localhost:8000/api/products";
    httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    })
  
    selectedUser
    constructor(private http : HttpClient) { }
    
    getAllProduct() : Observable<any>{
      return this.http.get(this.baseUrl + '/productList',
      {headers : this.httpHeaders});
    }
  
    getById(id) : Observable<any> {
      return this.http.get(this.baseUrl + '/productById/'+ id);
    }
  
    // addProduct(product) : Observable<any> {
    //   return this.http.post(this.baseUrl + '/createProduct',product);
    // }
     
    // update(product) {   
    //   var d= product.get('product_id');
    //   return this.http.put<Product>(this.baseUrl + '/editProduct/' + d, product)  
    
    // }  
  
    // delete(product) : Observable<any>{
    //   return this.http.delete(this.baseUrl + '/deleteProduct/' + product);
    // }

    
  
  }