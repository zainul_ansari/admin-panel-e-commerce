import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MymaterialModule } from  './mymaterial/mymaterial.module';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule, routingcomponents } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule } from 'ngx-bootstrap';


import { AppComponent } from './app.component';
import { ContentComponent } from './layouts/content/content.component';
import { FooterComponent } from './common/footer/footer.component';
import { CarouselComponent } from './layouts/carousel/carousel.component';
import { RegisterComponent } from './register/register.component';
import { ProductsComponent } from './layouts/products/products.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { Navbar2Component } from './common/navbar2/navbar2.component';
import { ContactsComponent } from './layouts/contacts/contacts.component';
import { AngnavbarComponent } from './common/angnavbar/angnavbar.component';
import { UserComponent } from './layouts/user/user.component';
import { CardsComponent } from './layouts/cards/cards.component';
import { CustomersComponent } from './customers/customers.component';
import { LoginComponent } from './shared/login/login.component';
import { HeaderComponent } from './common/header/header.component';
import { EdituserComponent} from './layouts/edituser/edituser.component';
import {SimpleComponent} from './layouts/simple/simple.component';
import { AngcarouselComponent } from './layouts/angcarousel/angcarousel.component';
// import {CarouselModule} from "angular2-carousel";
import { Product1Component } from './layouts/products/product1/product1.component';
import { Product2Component } from './layouts/products/product2/product2.component';
import { Product3Component } from './layouts/products/product3/product3.component';
import { ProductsliderComponent } from './layouts/productslider/productslider.component';
import { UserDialogComponent } from './customers/user-dialog/user-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    FooterComponent,
    CarouselComponent,
    RegisterComponent,
    routingcomponents,
    ProductsComponent,
    HomeComponent,
    NavbarComponent,
    Navbar2Component,
    ContactsComponent,
    AngnavbarComponent,
    UserComponent,
    CardsComponent,
    CustomersComponent,
    LoginComponent,
    HeaderComponent,
    EdituserComponent,
    SimpleComponent,
    AngcarouselComponent,
    Product1Component,
    Product2Component,
    Product3Component,
    ProductsliderComponent,
    UserDialogComponent
  ],
  entryComponents: [UserDialogComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MymaterialModule,
    CarouselModule.forRoot(),
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
