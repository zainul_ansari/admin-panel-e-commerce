export class User {

    constructor(
      public first_name: string,
      public username:string,
      public last_name: string,
      public email: string,
      public email2: string,
      public phone: string,
      public password: string,
      public id?: number
    ) {  }
  
  }