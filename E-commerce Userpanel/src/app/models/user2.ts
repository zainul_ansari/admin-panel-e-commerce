export class User {
    _id?: String;
    userName: String;
    password: String;
    firstName: String;
    lastName: String;
}
