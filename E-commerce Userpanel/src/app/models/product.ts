export class Product {

    constructor(
      public product_id: number,
      public name: string,
      public image: ImageData,
      public category: number,
      public category_name: string,
      public subCategory: number,
      public type: string,
      public color: string,
      public upload_date: string
    ) {  }
  
  }
  