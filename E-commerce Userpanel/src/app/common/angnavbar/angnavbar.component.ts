import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-angnavbar',
  templateUrl: './angnavbar.component.html',
  styleUrls: ['./angnavbar.component.css']
})
export class AngnavbarComponent implements OnInit {

  currentUser: any;
  loggedIn
  constructor(private router: Router,
    private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() { 
    this.loggedIn = JSON.parse(localStorage.getItem('loggedIn'))
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);

  }
}
