import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Carpet1Component } from './layouts/carpet1/carpet1.component';
import { Carpet2Component } from './layouts/carpet2/carpet2.component';
import { Carpet3Component } from './layouts/carpet3/carpet3.component';
import { Carpet4Component } from './layouts/carpet4/carpet4.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './shared/login/login.component';
import { HomeComponent } from 'src/app/home/home.component';
import { ContactsComponent } from './layouts/contacts/contacts.component';
import { AuthGuard } from './auth.guard';
import { UserComponent } from './layouts/user/user.component';
import { CardsComponent } from './layouts/cards/cards.component';
import { CustomersComponent } from './customers/customers.component';
import { EdituserComponent } from './layouts/edituser/edituser.component';
import { SimpleComponent } from './layouts/simple/simple.component';
import { AngnavbarComponent } from './common/angnavbar/angnavbar.component';
import { HeaderComponent } from './common/header/header.component';
import { AppComponent } from './app.component';
import { ProductsComponent } from './layouts/products/products.component';
import { Product1Component } from './layouts/products/product1/product1.component';
import { Product2Component } from './layouts/products/product2/product2.component';
import { Product3Component } from './layouts/products/product3/product3.component';
import { ContentComponent } from './layouts/content/content.component';
import { AngcarouselComponent } from './layouts/angcarousel/angcarousel.component';

const routes: Routes = [
  // { path: '', component: HomeComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'home', component: HomeComponent, children: [
      {path: 'home_content', component:ContentComponent},
      { path: 'user', component: CustomersComponent },
      { path: 'edit', component: EdituserComponent },
      { path: 'someform', component: SimpleComponent },
      { path: 'carpet1', component: Carpet1Component },
      { path: 'carpet2', component: Carpet2Component },
      { path: 'carpet3', component: Carpet3Component },
      { path: 'carpet4', component: Carpet4Component },
      { path: 'users',   component: UserComponent },
      { path: 'contact', component: ContactsComponent },
      { path: 'cards', component: CardsComponent },
      { path: 'sidebar', component: AngnavbarComponent },
      {path: 'prod_slider', component:AngcarouselComponent},
      {
        path: 'products', component: ProductsComponent, children: [
          { path: 'product1', component: Product1Component },
          { path: 'product2', component: Product2Component },
          { path: 'product3', component: Product3Component }
        ]
      }
    ]
  },


  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingcomponents =
  [
    Carpet1Component,
    Carpet2Component,
    Carpet3Component,
    Carpet4Component,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    ContactsComponent,
    CardsComponent
  ]