import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule, routingcomponents } from './app-routing.module';
import { MymaterialModule} from './mymaterial/mymaterial.module';
import { ToastrModule } from 'ngx-toastr';
import { ReactiveFormsModule } from '@angular/forms';
import { FileSelectDirective } from 'ng2-file-upload';

import { AppComponent } from './app.component';
import { LoginComponent } from './shared/login/login.component';
import { ForgetComponent } from './shared/forget/forget.component';
import { ResetComponent } from './shared/reset/reset.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './layouts/dashboard/dashboard.component';
import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { CategoryManagementComponent } from './layouts/category-management/category-management.component';
import { ProductManagementComponent } from './layouts/product-management/product-management.component';
import { OrderManagementComponent } from './layouts/order-management/order-management.component';
import { UserManagementComponent } from './layouts/user-management/user-management.component';
import { ProfileComponent } from './layouts/profile/profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserDialogComponent } from './layouts/user-management/user-dialog/user-dialog.component';
import { EdituserComponent } from './layouts/user-management/edituser/edituser.component';
import { AddproductComponent } from './layouts/product-management/addproduct/addproduct.component';
import { ViewproductComponent } from './layouts/product-management/viewproduct/viewproduct.component';
import { EditproductComponent } from './layouts/product-management/editproduct/editproduct.component';
import { AddCategoryComponent } from './layouts/category-management/add-category/add-category.component';
import { ViewDialogComponent } from './layouts/category-management/view-dialog/view-dialog.component';
import { SubCategoryManagementComponent } from './layouts/sub-category-management/sub-category-management.component';
import { AddSubComponent } from './layouts/sub-category-management/add-sub/add-sub.component';
import { ProfileSidebarComponent } from './layouts/profile/profile-sidebar/profile-sidebar.component';
import { EditProfileComponent } from './layouts/profile/edit-profile/edit-profile.component';
import { ProfileInfoComponent } from './layouts/profile/profile-info/profile-info.component';
import { ProfileAlbumComponent } from './layouts/profile/profile-album/profile-album.component';
import { ChangePasswordComponent } from './layouts/profile/change-password/change-password.component';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from "angularx-social-login";
import { SocialInfoComponent } from './layouts/social-info/social-info.component';
import { OtpVerifyComponent } from './shared/otp-verify/otp-verify.component';
import { EditSubcategoryComponent } from './layouts/sub-category-management/edit-subcategory/edit-subcategory.component';
import { EditCategoryComponent } from './layouts/category-management/edit-category/edit-category.component';
const facebook_oauth_client_id: string = '2584931924879436';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('624796833023-clhjgupm0pu6vgga7k5i5bsfp6qp6egh.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(facebook_oauth_client_id)
  },
  {
    id: LinkedInLoginProvider.PROVIDER_ID,
    provider: new LinkedInLoginProvider('78iqy5cu2e1fgr')
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    FileSelectDirective,
    AppComponent,
    LoginComponent,
    ForgetComponent,
    routingcomponents,
    ResetComponent,
    RegisterComponent,
    DashboardComponent,
    SidebarComponent,
    CategoryManagementComponent,
    ProductManagementComponent,
    OrderManagementComponent,
    UserManagementComponent,
    ProfileComponent,
    UserDialogComponent,
    EdituserComponent,
    AddproductComponent,
    ViewproductComponent,
    EditproductComponent,
    AddCategoryComponent,
    ViewDialogComponent,
    SubCategoryManagementComponent,
    AddSubComponent,
    ProfileSidebarComponent,
    EditProfileComponent,
    ProfileInfoComponent,
    ProfileAlbumComponent,
    ChangePasswordComponent,
    SocialInfoComponent,
    OtpVerifyComponent,
    EditSubcategoryComponent,
    EditCategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MymaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    SocialLoginModule,
    ToastrModule.forRoot()
  ],
  entryComponents: [
    UserDialogComponent,
  ],
  providers: [ {
    provide: AuthServiceConfig,
    useFactory: provideConfig
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
