import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Auth2Service } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private auth2Service: Auth2Service
) {}

canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
  let url: string = state.url;

  return this.checkLogin(url);
}
checkLogin(url: string): boolean {
  if (this.auth2Service.login) { return true; }
  this.auth2Service.baseUrl = url;
  this.router.navigate(['/login']);
  return false;
}
}
