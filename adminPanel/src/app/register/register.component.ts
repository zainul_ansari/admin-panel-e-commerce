import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiService} from '../services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[ApiService]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(
    private router: Router, 
    private formBuilder: FormBuilder, 
    private apiService: ApiService) { }
  
    first_name:string;
    last_name:string;
    email: string;
    email2:string;
    phone:string;
    password: string;
    account_type;
    social_id;

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      email2: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required,Validators.minLength(10)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });
  }

  get f() { return this.registerForm.controls; }

  public hasError = (controlName: string, errorName: string) =>{
    return this.registerForm.controls[controlName].hasError(errorName);
  }
  
  onSubmit() {
      this.submitted = true;
      this.account_type =1;
      this.social_id =1;
      if (this.registerForm.invalid) {
          return;
      }

      let dataToSend = {
        first_name:this.registerForm.value.first_name,
        last_name:this.registerForm.value.last_name,
        email : this.registerForm.value.email,
        email2 : this.registerForm.value.email2,
        phone : this.registerForm.value.phone,
        password : this.registerForm.value.password,
        account_type:this.account_type,
        social_id:this.social_id
      };
      if(this.registerForm.valid){
        this.apiService.register(dataToSend).subscribe((response)=>{
          console.log('repsonse ',response);
          this.router.navigate(["login"]);
        });
        }

      // display form values on success
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  onReset() {
      this.submitted = false;
      this.registerForm.reset();
  }
}