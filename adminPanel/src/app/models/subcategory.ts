export class SubCategory {

    constructor(
      public name: string,
      public isActive:boolean,
      public image: any,
      public category:any,
      public category_name:string,
      public id?: number
    ) {  }
  
  }