export class Category {

    constructor(
      public name: string,
      public isActive:boolean,
      public image:ImageData,
      public upload_date: string,
      public id?: number
    ) {  }
  
  }