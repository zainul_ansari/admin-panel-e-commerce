import { Component, OnInit } from '@angular/core';
import { Auth2Service } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { ApiService} from '../../services/api.service';
import { AuthService } from 'angularx-social-login';
import { ToastrService } from 'ngx-toastr';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [Auth2Service, ApiService]
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  loading = false;
  returnUrl: string;
  error: string;
  email: string;
  password: string;
  loggedIn = false
  currentUser;
  token;
  showElement = true;
  
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private auth2Service: Auth2Service,
    private authservice: AuthService,
    private apiservice:ApiService,
    private toast:ToastrService) {
    // redirect to home if already logged in
    if (this.auth2Service.currentUserValue) {
      this.router.navigate(['admin']);
    }
  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.loginForm.controls; }
  public hasError = (controlName: string, errorName: string) => {
    return this.loginForm.controls[controlName].hasError(errorName);
  }

  login() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.auth2Service.login(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.loggedIn = true
          localStorage.setItem('loggedIn', JSON.stringify(this.loggedIn))
          this.toast.success('Logged In succesfully', 'success');
          this.router.navigate(['admin']);
        },
        error => {
          this.toast.error('Invalid Credentials',error.message);
          this.loading = false;
        });
  }
  signInWithGoogle(): void {
    this.authservice.signIn(GoogleLoginProvider.PROVIDER_ID)
    .then(x => {
      let data = {
        first_name:x.firstName,
        last_name:x.lastName,
        email:x.email,
        email2:x.email,
        social_id:x.id,
        account_type:2,
        phone:'',
        password:''
      }
      this.apiservice.register(data)
      .subscribe((response)=>{
        console.log('repsonse ',response);
        for(var key in response){
          var val = response[key].token;
          console.log(val);  
      }
      setTimeout(() => {
        this.currentUser = response;
        localStorage.setItem('currentUser', JSON.stringify(this.currentUser))
        this.loggedIn = true
        localStorage.setItem('loggedIn', JSON.stringify(this.loggedIn))
        this.router.navigate(['admin']);
        }, 4000);
      });
    });
    this.router.navigate(['socialInfo']);
  }
  
  signInWithFB(): void {
    this.authservice.signIn(FacebookLoginProvider.PROVIDER_ID)
    .then(x => {
      let data = {
        first_name:x.firstName,
        last_name:x.lastName,
        email:x.email,
        email2:x.email,
        social_id:x.id,
        account_type:2,
        phone:'',
        password:''
      }
      this.apiservice.register(data)
      .subscribe((response)=>{
        console.log('repsonse ',response);
        for(var key in response){
          var val = response[key].token;
          console.log(val);  
      }
      setTimeout(() => {
        this.currentUser = response;
        localStorage.setItem('currentUser', JSON.stringify(this.currentUser))
        this.loggedIn = true
        localStorage.setItem('loggedIn', JSON.stringify(this.loggedIn))
        this.router.navigate(['admin']);
        }, 4000);
      });
    });
    this.router.navigate(['socialInfo']);
  } 
  
  signInWithLinkedIn(): void {
    this.authservice.signIn(LinkedInLoginProvider.PROVIDER_ID);
    this.router.navigate(['socialInfo']);
  }

}
