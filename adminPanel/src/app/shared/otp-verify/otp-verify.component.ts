import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp-verify',
  templateUrl: './otp-verify.component.html',
  styleUrls: ['./otp-verify.component.css']
})
export class OtpVerifyComponent implements OnInit {
  otp_number = new FormControl('', Validators.required);

  getErrorMessage() {
    return this.otp_number.hasError('required') ? 'You must enter a value' : '';
    }
  
  constructor(private api: ApiService, private toast: ToastrService, private router: Router) { }

  ngOnInit() {
  }

  verifyOtp() {
    
    if (!this.otp_number.value) {
      this.toast.error('Type in your otp_number first', 'error');
    }
    
    if (this.otp_number.value) {
      let data = {
        otp_value : this.otp_number.value,
        user:JSON.parse(localStorage.getItem('userAfterOtp')).user
      }
      console.log(data)
      this.api.otp_verify(data).subscribe(
        (x) => {
          this.toast.success('A password reset link has been sent to your email address', 'success')
          this.router.navigate(['reset']);
        },
        (err) => {
          this.toast.error('An error occurred while attempting to reset your password', 'error')
        }
      )
    }
  }


}
