import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {
   
    new_password = new FormControl('', [Validators.required, Validators.minLength(8)]);
    conf_password = new FormControl('', [Validators.required, Validators.minLength(8)]);
    getErrorMessage() {
      return this.new_password.hasError('required') ? 'You must enter a value' :
        this.new_password.hasError('minLength') ? 'minimum length must be 10' :
          '';
      }
    
    constructor(private api: ApiService, private toast: ToastrService, private router: Router) { }
  
    ngOnInit() {
    }
  
    reset() {
      var leng1 = (this.new_password.value).length;
      var leng2 = (this.conf_password.value).length;
  
      if (!this.new_password.value) {
        this.toast.error('Type in your new password', 'error');
      }

      if (this.new_password.value && (leng1 && leng2 < 10)) {
        this.toast.error('both passwords must atleast 8 character long', 'error');
      }
  
      if (this.new_password.value && (leng1 && leng2 > 9)) {
        let data = {
          new_password: this.new_password.value,
          conf_password: this.conf_password.value,
          user_id:JSON.parse(localStorage.getItem('userAfterOtp')).user
        }
        this.api.resetPassword(data).subscribe(
          (x) => {
            this.toast.success('Your password Changed Successfully', 'success')
            this.router.navigate(['login']);
            localStorage.removeItem('userAfterOtp');
          },
          (err) => {
            this.toast.error('An error occurred while attempting to reset your password', 'error')
          }
        )
      }
    }
  
  
  }
  