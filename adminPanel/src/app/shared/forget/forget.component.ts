import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.css']
})
export class ForgetComponent implements OnInit {
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
  phonepattern = "[0-9]*"
  user_id;
  email = new FormControl('', [Validators.required, Validators.minLength(10), Validators.pattern(this.emailPattern)]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('minLength') ? 'minimum length must be 10' :
      this.email.hasError('pattern')? 'Not valid email': 
        '';
    }
  
  constructor(private api: ApiService, private toast: ToastrService, private router: Router) { }

  ngOnInit() {
  }

  sendRequest() {
    // var leng = (this.email.value).length;

    if (!this.email.value) {
      this.toast.error('Type in your email first', 'error');
    }
    // if (this.email_or_phone.value && leng < 10) {
    //   this.toast.error('phone number must be of 10 character long', 'error');
    // }

    if (this.email.value) {
      let data={
        email :this.email.value
      } 
      
      this.api.sendRequest(data).subscribe(
        (x) => {
          localStorage.setItem('userAfterOtp', JSON.stringify(x))
          this.toast.success('A password reset link has been sent to your email address', 'success')
          this.router.navigate(['otp']);
        },
        (err) => {
          this.toast.error('An error occurred while attempting to reset your password', 'error')
        }
      )
    }
  }


}
