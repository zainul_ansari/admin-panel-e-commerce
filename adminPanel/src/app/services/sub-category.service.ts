import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SubCategory } from '../models/subcategory';

@Injectable({
  providedIn: 'root'
})
export class SubCategoryService {

  baseUrl = "http://localhost:8000/api/products";
  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  })

  selectedUser
  constructor(private http : HttpClient) { }
  
  getAllSubCat(id) : Observable<any>{
    return this.http.get(this.baseUrl + '/subList/' +id);
  }
  
  getSubCatDetails(): Observable<any>{
    return this.http.get(this.baseUrl + '/subCatDetails')
  }

  getById(id) : Observable<any> {
    return this.http.get(this.baseUrl + '/subcatById/'+ id);
  }

  addSubCat(sub) : Observable<any> {
    return this.http.post(this.baseUrl + '/createSub',sub);
  }
  
  update(sub) {  
    console.log(sub);  
    const params = new HttpParams().set('id', sub.id);  
    const headers = new HttpHeaders().set('content-type', 'application/json');  
    var body = {  
      name: sub.name, isActive: sub.isActive, upload_date: sub.upload_date, id: sub.id  
      }  
    return this.http.put<SubCategory>(this.baseUrl + '/edit/' + sub.id, body, { headers, params })  
  
  }  

  delete(sub) : Observable<any>{
    return this.http.delete(this.baseUrl + '/deleteSub/' + sub);
  }

}
