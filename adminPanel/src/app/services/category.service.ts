import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  baseUrl = "http://localhost:8000/api/products";
  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  })

  selectedUser
  constructor(private http : HttpClient) { }
  
  getAllCat() : Observable<any>{
    return this.http.get(this.baseUrl + '/catList',
    {headers : this.httpHeaders});
  }
  
  getCatDetails() : Observable<any>{
    return this.http.get(this.baseUrl + '/catDetails',
    {headers : this.httpHeaders});
  }
  // getById(id) : Observable<any> {
  //   return this.http.get(this.baseUrl + '/users_details/'+ id);
  // }

  addCat(cat) : Observable<any> {
    return this.http.post(this.baseUrl + '/createCat',cat);
  }
  
  update(cat) {  
    console.log(cat);  
    const params = new HttpParams().set('id', cat.id);  
    const headers = new HttpHeaders().set('content-type', 'application/json');  
    var body = {  
      name: cat.name, isActive: cat.isActive, upload_date: cat.upload_date, id: cat.id  
      }  
    return this.http.put<Category>(this.baseUrl + '/edit/' + cat.id, body, { headers, params })  
  
  }  

  delete(cat) : Observable<any>{
    return this.http.delete(this.baseUrl + '/deleteCat/' + cat);
  }

}
