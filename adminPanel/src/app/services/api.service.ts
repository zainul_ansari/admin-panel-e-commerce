import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { Auth2Service} from './auth.service';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl = "http://localhost:8000/api/accounts";
  
  token = localStorage.getItem('currentUser') != undefined ? JSON.parse(localStorage.getItem('currentUser')).data.token : "";
  
  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  })
  tokenHeader = new HttpHeaders({
    'Content-Type' : 'application/json',
    'Authorization': this.token
  })
  
  

  selectedUser
  constructor(private http : HttpClient) {

  }
  
  getAllUsers() : Observable<any>{
    return this.http.get(this.baseUrl + '/users_lists',
    {headers : this.httpHeaders})
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getById(id) : Observable<any> {
    return this.http.get(this.baseUrl + '/users_details/'+ id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  
  getUserProfle() : Observable<any> {
    console.log(this.token)
    return this.http.get(this.baseUrl + '/userProfile',
    {headers : this.tokenHeader})
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  changePassword(body):Observable<any>{
    return this.http.post(this.baseUrl + '/changePass', body,
    {headers : this.tokenHeader})
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  sendRequest(email){
    return this.http.post(this.baseUrl + '/sendMail',email)
  }

  otp_verify(body): Observable<any>{
    return this.http.post(this.baseUrl +'/otp_verify',body)
  }

  resetPassword(body){
    return this.http.post(this.baseUrl + '/reset', body)
  }
  
  register(user) : Observable<any> {
    return this.http.post(this.baseUrl + '/register/',user)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  
  updateAdmin(user){    
      var body = {  
        first_name: user.first_name, last_name: user.last_name, email: user.email, phone: user.phone 
        }  
      return this.http.post<User>(this.baseUrl + '/userProfile', body,
      {headers : this.tokenHeader})
      .pipe(
        retry(1),
        catchError(this.handleError)
      );  
        
    }

  update(user) {  
    const params = new HttpParams().set('id', user.id);  
    const headers = new HttpHeaders().set('content-type', 'application/json');  
    var body = {  
      username: user.username, email: user.email, phone: user.phone, password: user.password, id: user.id  
      }  
    return this.http.put<User>(this.baseUrl + '/edit/' + user.id, body, { headers, params })
    .pipe(
      retry(1),
      catchError(this.handleError)
    );  
  
  }  

  delete(user) : Observable<any>{
    return this.http.delete(this.baseUrl + '/delete/' + user)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

