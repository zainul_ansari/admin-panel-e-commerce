import { NgModule } from '@angular/core';
import { Routes, CanActivate,RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './shared/login/login.component';
import { DashboardComponent } from './layouts/dashboard/dashboard.component';
import { CategoryManagementComponent } from './layouts/category-management/category-management.component';
import { OrderManagementComponent } from './layouts/order-management/order-management.component';
import { ProductManagementComponent } from './layouts/product-management/product-management.component';
import { Profile } from 'selenium-webdriver/firefox';
import { ProfileComponent } from './layouts/profile/profile.component';
import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { UserManagementComponent } from './layouts/user-management/user-management.component';
import { EdituserComponent } from './layouts/user-management/edituser/edituser.component';
import { AddproductComponent } from './layouts/product-management/addproduct/addproduct.component';
import { EditproductComponent } from './layouts/product-management/editproduct/editproduct.component';
import { ViewproductComponent } from './layouts/product-management/viewproduct/viewproduct.component';
import { AddCategoryComponent } from './layouts/category-management/add-category/add-category.component';
import { SubCategoryManagementComponent } from './layouts/sub-category-management/sub-category-management.component';
import { AddSubComponent } from './layouts/sub-category-management/add-sub/add-sub.component';
import { ProfileInfoComponent } from './layouts/profile/profile-info/profile-info.component';
import { EditProfileComponent } from './layouts/profile/edit-profile/edit-profile.component';
import { ProfileAlbumComponent } from './layouts/profile/profile-album/profile-album.component';
import { SocialInfoComponent } from './layouts/social-info/social-info.component';
import { ChangePasswordComponent } from './layouts/profile/change-password/change-password.component';
import { ForgetComponent } from './shared/forget/forget.component';
import { OtpVerifyComponent } from './shared/otp-verify/otp-verify.component';
import { ResetComponent } from './shared/reset/reset.component';
import {LazyModule} from './lazy.module';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  {path: 'socialInfo', component:SocialInfoComponent},
  {path: 'forget-password', component:ForgetComponent},
  {path:'otp',component:OtpVerifyComponent},
  {path: 'reset', component:ResetComponent},
  {path:'lazy', loadChildren:'./lazy.module#LazyModule'},

  {path:'admin', component:DashboardComponent,canActivate: [AuthGuard] , children:[ 
    { path: '', redirectTo: 'user', pathMatch: 'full' },
    {path:'category', component:CategoryManagementComponent},
    {path: 'addCategory', component:AddCategoryComponent},
    {path: 'subCategory', component:SubCategoryManagementComponent},
    {path: 'addSub', component:AddSubComponent},
    {path:'order', component:OrderManagementComponent},
    {path:'product', component:ProductManagementComponent},
    {path:'addproduct', component:AddproductComponent},
    {path:'editproduct', component:EditproductComponent},
    {path:'viewproduct', component:ViewproductComponent},
    {path:'profile', component:ProfileComponent, children:[
      {path:'profileInfo', component:ProfileInfoComponent},
      {path:'', redirectTo: 'profileInfo', pathMatch: 'full'},
      {path:'editProfile', component:EditProfileComponent},
      {path:'contactInfo', component:ProfileInfoComponent},
      {path:'change', component:ChangePasswordComponent},
      {path:'album', component:ProfileAlbumComponent}
    ]},
    {path:'sidebar', component:SidebarComponent},
    {path:'user', component:UserManagementComponent},
    {path:'edit', component:EdituserComponent}
   ]},
   // otherwise redirect to home
   { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingcomponents =
  [  ]
