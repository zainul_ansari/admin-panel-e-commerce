import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder,FormControl ,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var $:any;

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  panelOpenState = false;
  editProfile: FormGroup;
  submitted = false;
  // userID;
  id;
  first_name;
  last_name;
  phone;
  email;
  token;
  gender="male"
  userDetails;
  apiEmail;
  apiId;

  changeForm = new FormGroup({
    old_password: new FormControl('', Validators.required),
    new_password: new FormControl('', Validators.required),
    conf_password: new FormControl('', Validators.required)
 });
  constructor(private api:ApiService,
    private route: ActivatedRoute,
    private toast:ToastrService,
    private router: Router,
    private apiService: ApiService,
    private fb: FormBuilder) {
    this.createForm();
  }
  createForm() {
    this.editProfile = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.userProfile()  
  }

  userProfile(){
    this.api.getUserProfle().subscribe((res:any)=>{
      this.userDetails = res;
      for(var key in this.userDetails){
          this.first_name = this.userDetails[key].first_name
          this.last_name = this.userDetails[key].last_name
          this.email = this.userDetails[key].email
          this.phone = this.userDetails[key].phone
        }
    
        this.editProfile.setValue({
          first_name: this.first_name,
          last_name: this.last_name,
          email: this.email,
          phone: this.phone
        });
      })
  }

  updateDetailsProfile(){
      this.submitted = true;
  
      // stop here if form is invalid
      if (this.editProfile.invalid) {
        return;
      }
      let dataToSend = {
        first_name: this.editProfile.value.first_name,
        last_name: this.editProfile.value.last_name,
        email: this.editProfile.value.email,
        phone: this.editProfile.value.phone
      };
  
      if (this.editProfile.valid) {
        this.apiService.updateAdmin(dataToSend).subscribe(
          data => {
            console.log(data);
            this.toast.success(data['messege'], 'Success!');
            this.router.navigate(['/admin/profile/editProfile']);
          },
          error => {
            this.toast.error(error['error']['message']);
            console.log(error);
          }
        );
      }

}
showModel(){
  $('#change-pass').modal('show');
}

save(){
  if(this.changeForm.invalid){
    this.toast.error('please fillup all fields','error')
    return ;
  }
  var leng = this.changeForm.value.old_password.length && this.changeForm.value.new_password.length && this.changeForm.value.conf_password.length;
  console.log(leng)
  if(this.changeForm.valid && leng < 8){
    this.toast.error('Password must be atleast 8 character long')
  }
  if(this.changeForm.value.conf_password != this.changeForm.value.new_password){
    this.toast.error('confirm Password must be same as password','error')
  }
  let data = {
    old_password: this.changeForm.value.old_password,
    new_password: this.changeForm.value.new_password,
    conf_password: this.changeForm.value.conf_password
    };

  if(this.changeForm.valid && leng > 7){
    this.api.changePassword(data).subscribe(
      (x)=>{
        console.log(x);
        this.toast.success('saved succesfully', 'success');
        $('#change-pass').modal('hide');
        // this.router.navigate(['/admin/profile/profileInfo']);
      },
      (error)=>{
        this.toast.error('Bad Request from server', 'error');
        $('#change-pass').modal('hide');
        console.log(error);
        // this.router.navigate(['/admin/profile/profileInfo']);
      }
    )
  }
}

}
