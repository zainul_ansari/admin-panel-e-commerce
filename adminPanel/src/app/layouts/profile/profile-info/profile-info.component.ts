import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../services/api.service';
@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.css']
})
export class ProfileInfoComponent implements OnInit {
  first_name;
  last_name;
  phone;
  email;
  token;
  gender="male"
  userDetails;
  apiEmail;
  apiId;
  constructor(private api:ApiService) { }

  ngOnInit() {
    this.userProfile()  
  }
  userProfile(){
    this.api.getUserProfle().subscribe((res:any)=>{
      this.userDetails = res;
      for(var key in this.userDetails){
          this.first_name = this.userDetails[key].first_name
          this.last_name = this.userDetails[key].last_name
          this.email = this.userDetails[key].email
          this.phone = this.userDetails[key].phone
        }
    })
  }
  // getUserDetails(){
  //   this.api.getAllUsers()
  //     .subscribe((response: any) => {
  //       this.userDetails = response
  //       for(var key in this.userDetails){
  //         this.apiEmail = this.userDetails[key].email
  //       }
  //     },
  //       error => {
  //         alert(error);
  //       })
  // }

}
