import {Component, OnInit, ViewChild} from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
// import { DialogService } from './user-dialog/dialog.service';
 
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];


@Component({
  selector: 'app-category-management',
  templateUrl: './category-management.component.html',
  styleUrls: ['./category-management.component.css'],
  providers: [
    CategoryService,
    // DialogService
  ]
})
export class CategoryManagementComponent implements OnInit {
  userData;
  displayedColumns: string[] = ['id', 'name', 'isActive', 'image', 'action'];
  dataSource= new  MatTableDataSource
  color= COLORS[Math.round(Math.random() * (COLORS.length - 1))];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private categoryService: CategoryService, private toastr: ToastrService) {
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(users);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.listing()
  }

  listing() {
    this.categoryService.getCatDetails()
      .subscribe((response: any) => {
        this.userData = response;
        this.dataSource = new MatTableDataSource(this.userData)
      },
        error => {
          alert(error);
        })
  }
  delete(cat) {
    if (confirm('Are you sure to delete this record ?') == true) {
        let index = this.userData.indexOf(cat);
      this.userData.splice(index, 1);
      this.dataSource = new MatTableDataSource(this.userData)
      
      // this.selectedRemoveUser = user;
      this.categoryService.delete(cat.id)
        .subscribe(x => {
          this.categoryService.getCatDetails();
          this.toastr.warning("Deleted Successfully", "Registered user");
        }, error => {
          alert(error);
        });
      }
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

