import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ProductService } from '../../../services/product.service';
import { CategoryService} from '../../../services/category.service';
import { SubCategoryService} from '../../../services/sub-category.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css'],
  providers:[ProductService, CategoryService, SubCategoryService]
})
export class AddCategoryComponent implements OnInit {

  categoryForm: FormGroup;
  submitted = false;
  fileData = null;
  selectedFile: File
  
  constructor(
    private router: Router,  
    private categoryService:CategoryService) {  }

  ngOnInit() {
    this.categoryForm = new FormGroup({
      name: new FormControl(),
      isActive :new FormControl(false)
   });
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
  }

  addCategory() {
    this.submitted = true;
      const dataToSend = new FormData();
      dataToSend.append('name', this.categoryForm.value.name),
      dataToSend.append('isActive', this.categoryForm.value.isActive),
      dataToSend.append('image',this.selectedFile)
      
    this.categoryService.addCat(dataToSend).subscribe((response)=>{
        console.log('repsonse ',response);
        this.router.navigate(["/admin/category"]);
      });
      
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.categoryForm.value, null, 4));
}

onReset() {
    this.submitted = false;
    this.categoryForm.reset();
}
}
