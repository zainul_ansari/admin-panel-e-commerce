import { Component, OnInit } from '@angular/core';
import { Auth2Service } from '../../services/auth.service';
import {ApiService} from '../../services/api.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormControl, FormGroup, Validator, Validators} from '@angular/forms';
declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  value = "users";
  showFiller = false;
  title = "";
  email ='user@gmail.com';
  currentUser: any;
  loggedIn
  changeForm = new FormGroup({
    old_password: new FormControl('', Validators.required),
    new_password: new FormControl('', Validators.required),
    conf_password: new FormControl('', Validators.required)
 });
  

  constructor(private router: Router,
    private toastr: ToastrService,
    private api:ApiService,
    private auth2Service: Auth2Service) {
    this.auth2Service.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  
  }
  
  setTitle( newTitle: string) {
    this.title = newTitle;
    localStorage.setItem('title', JSON.stringify(this.title))
  }
  ngOnInit() { 
    this.loggedIn = JSON.parse(localStorage.getItem('loggedIn'))
    this.title = JSON.parse(localStorage.getItem('title'))
    this.email = JSON.parse(localStorage.getItem('currentUser')).data.email;
    console.log(this.email);
  }
  showModel(){
    $('#change-password').modal('show');
  }
  save(){
    if(this.changeForm.invalid){
      this.toastr.error('please fillup all fields','error')
      return ;
    }
    var leng = this.changeForm.value.old_password.length && this.changeForm.value.new_password.length && this.changeForm.value.conf_password.length;
    console.log(leng)
    if(this.changeForm.valid && leng < 8){
      this.toastr.error('Password must be atleast 8 character long')
    }
    if(this.changeForm.value.conf_password != this.changeForm.value.new_password){
      this.toastr.error('confirm Password must be same as password','error')
    }
    let data = {
      old_password: this.changeForm.value.old_password,
      new_password: this.changeForm.value.new_password,
      conf_password: this.changeForm.value.conf_password
      };
  
    if(this.changeForm.valid && leng > 7){
      this.api.changePassword(data).subscribe(
        (x)=>{
          console.log(x);
          this.toastr.success('saved succesfully', 'success');
          $('#change-password').modal('hide');
          // this.router.navigate(['/'])
        },
        (error)=>{
          this.toastr.error('Bad Request from server', 'error');
          $('#change-password').modal('hide');
        }
      )
    }
  }
  

  logout() {
    this.auth2Service.logout();
    localStorage.removeItem('loggedIn');
    localStorage.removeItem('title');
    localStorage.removeItem('googleLoggedIn');
    this.title = "Dashboard"
    this.router.navigate(['/login']);

  }
}