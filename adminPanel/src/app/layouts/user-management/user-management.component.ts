import { Component, OnInit ,Inject} from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DialogService } from './user-dialog/dialog.service';
 
@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css'],
  providers: [ApiService, DialogService]
})
export class UserManagementComponent implements OnInit {
 
  userData
  dataSource = new MatTableDataSource
  displayedColumns = ['sr_no', 'id', 'username', 'email', 'phone', 'action'];
  selectedRemoveUser = {};
  

  constructor(
    private apiService: ApiService, 
    private toastr: ToastrService,
    private DialogService: DialogService) {
    }


  ngOnInit() {
    this.listing()
  }

  listing() {
    this.apiService.getAllUsers()
      .subscribe((response: any) => {
        this.userData = response
        this.dataSource = new MatTableDataSource(this.userData)
      },
        error => {
          alert(error);
        })
  }
  

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  openDialog(id){
    this.DialogService.selectViewUser(id);
  }

  deleteUser(user) {
    if (confirm('Are you sure to delete this record ?') == true) {
        let index = this.userData.indexOf(user);
      this.userData.splice(index, 1);
      this.dataSource = new MatTableDataSource(this.userData)
      
      // this.selectedRemoveUser = user;
      this.apiService.delete(user.id)
        .subscribe(x => {
          this.apiService.getAllUsers();
          this.toastr.warning("Deleted Successfully", "Registered user");
        }, error => {
          alert(error);
        });
      }
  }

  
      
}