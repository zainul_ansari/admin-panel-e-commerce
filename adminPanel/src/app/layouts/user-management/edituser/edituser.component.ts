import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../../models/user';
import { ApiService } from '../../../services/api.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {

  element: any = {};
  editForm: FormGroup;
  submitted = false;
  username: string;
  email: string;
  phone: string;
  user;
  userID;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private toast: ToastrService,
    private fb: FormBuilder) {
    this.createForm();
  }
  createForm() {
    this.route.params.subscribe(data => {
      this.userID = data.id;
      this.getUser(data.id);
    });
    this.editForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  getUser(id) {
    this.apiService.getById(id)
      .subscribe((response: any) => {
        console.log(response);
        //  this.apiService.getAllUsers();
        this.editForm.setValue({
          username: response.username,
          email: response.email,
          phone: response.phone,
          password:response.password
        });
      }),
      error => {
        alert(error);
      }
  }
  ngOnInit() {
    //   this.route.params.subscribe(params => {
    //     this.apiService.getById(params['id']).subscribe(res => {
    //       this.element = res;
    //   });
    // });
  }



  get f() { return this.editForm.controls; }
  public hasError = (controlName: string, errorName: string) => {
    return this.editForm.controls[controlName].hasError(errorName);
  }
  updateUser() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }
    let dataToSend = {
      id: this.userID,
      username: this.editForm.value.username,
      email: this.editForm.value.email,
      phone: this.editForm.value.phone,
      password:this.editForm.value.password
    };

    if (this.editForm.valid) {
      this.apiService.update(dataToSend).subscribe(
        data => {
          this.toast.success(data['message']);
          this.router.navigate(['/admin/user']);
        },
        error => {
          this.toast.error(error['error']['message']);
          console.log(error);
        }
      );

      //   this.route.params.subscribe(params => {
      //     this.apiService.editUser(params['id']);
      //     this.router.navigate(['user']);
      //  });
    }

  }
}