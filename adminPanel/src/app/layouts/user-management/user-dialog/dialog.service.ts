import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../../../services/api.service';
import { ToastrService } from 'ngx-toastr';
import {UserDialogComponent} from '../user-dialog/user-dialog.component';
import { User } from '../../../models/user';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  id;
  username;
  email;
  phone;
  password;
  constructor(public dialog: MatDialog,
    private apiService: ApiService,
    private toastr: ToastrService) { }

  selectViewUser(id): void {
    this.apiService.getById(id)
      .subscribe((response: any) => {
        const dialogRef = this.dialog.open(UserDialogComponent, {
          width: '500px',
          data: {
            id: response.id,
            username: response.username,
            email: response.email,
            phone: response.phone,
            password: response.password
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          this.toastr.warning("Dialog Closed", result);
        });
      }),
      error => {
        alert(error);
      }
  }

}
