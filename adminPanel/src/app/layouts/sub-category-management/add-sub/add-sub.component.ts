import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CategoryService} from '../../../services/category.service';
import { SubCategoryService} from '../../../services/sub-category.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-sub',
  templateUrl: './add-sub.component.html',
  styleUrls: ['./add-sub.component.css'],
  providers:[CategoryService, SubCategoryService]
})
export class AddSubComponent implements OnInit {

  subCatForm: FormGroup;
  submitted = false;
  category:[];
  fileData = null;
  selectedFile: File
  
  constructor(private route: ActivatedRoute,
    private router: Router, 
    private formBuilder: FormBuilder, 
    private http: HttpClient,
    private categoryService:CategoryService,
    private subcategoryService:SubCategoryService) {
      
     }


  ngOnInit() {
    this.subCatForm = new FormGroup({
      name: new FormControl(),
      isActive: new FormControl(false),
      category: new FormControl()
   });
   this.getAllCategories()
  }

    getAllCategories(){
      this.categoryService.getAllCat().subscribe(res =>{
        this.category = res;
        console.log(this.category);
      });
    }
    getAllSubCategories(id){
      var cat_id = id;
    }


  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
  }

  addSubCat() {
    this.submitted = true;
      const dataToSend = new FormData();
      dataToSend.append('name', this.subCatForm.value.name),
      dataToSend.append('isActive',this.subCatForm.value.isActive),
      dataToSend.append('image',this.selectedFile),
      dataToSend.append('category', this.subCatForm.value.category)
    
      this.subcategoryService.addSubCat(dataToSend).subscribe((response)=>{
        console.log('repsonse ',response);
        this.router.navigate(["/admin/subCategory"]);
      });
      
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.subCatForm.value, null, 4));
}

onReset() {
    this.submitted = false;
    this.subCatForm.reset();
}
}
