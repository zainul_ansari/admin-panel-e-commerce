import {Component, OnInit, ViewChild} from '@angular/core';
import { SubCategoryService } from '../../services/sub-category.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
// import { DialogService } from './user-dialog/dialog.service';
import { Product } from '../../models/product'; 
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];

@Component({
  selector: 'app-sub-category-management',
  templateUrl: './sub-category-management.component.html',
  styleUrls: ['./sub-category-management.component.css'],
  providers: [
    SubCategoryService,
    // DialogService
  ]
})
export class SubCategoryManagementComponent implements OnInit {

  userData;
  displayedColumns: string[] = [
    'id', 
    'name', 
    'isActive',
    'image', 
    'category',
    'category_name',
    'action'];
  dataSource= new  MatTableDataSource
  color= COLORS[Math.round(Math.random() * (COLORS.length - 1))];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private subcatService: SubCategoryService, private toastr: ToastrService) {  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.listing()
  }

  listing() {
    this.subcatService.getSubCatDetails()
      .subscribe((response: any) => {
        this.userData = response
        this.dataSource = new MatTableDataSource(this.userData)
      },
        error => {
          alert(error);
        })
  }
  delete(sub) {
    if (confirm('Are you sure to delete this record ?') == true) {
        let index = this.userData.indexOf(sub);
      this.userData.splice(index, 1);
      this.dataSource = new MatTableDataSource(this.userData)
      
      // this.selectedRemoveUser = user;
      this.subcatService.delete(sub.id)
        .subscribe(x => {
          this.subcatService.getSubCatDetails();
          this.toastr.warning("Deleted Successfully", "Registered user");
        }, error => {
          alert(error);
        });
      }
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
}