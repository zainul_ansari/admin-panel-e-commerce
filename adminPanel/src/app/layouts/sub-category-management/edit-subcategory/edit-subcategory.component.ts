import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../../models/user';
import { CategoryService } from '../../../services/category.service';
import { SubCategoryService } from '../../../services/sub-category.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-subcategory',
  templateUrl: './edit-subcategory.component.html',
  styleUrls: ['./edit-subcategory.component.css']
})
export class EditSubcategoryComponent implements OnInit {
  element: any = {};
  editForm: FormGroup;
  submitted = false;
  category:[];
  subCategory:[];
  fileData = null;
  selectedFile: File
  username: string;
  product_id;
  name; 
  image;
  type;
  color;
  upload_date;
  user;
  productID;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private catService: CategoryService,
    private subcatService: SubCategoryService,
    private toast: ToastrService,
    private fb: FormBuilder) {
    this.createForm();
  }
  createForm() {
    this.route.params.subscribe(data => {
      this.productID = data.id;
      this.getProduct(data.id);
      // console.log('product id'+ data.id)
    });
    this.editForm = this.fb.group({ 
      name:['', Validators.required],
      image:['', Validators.required],
      category:['', Validators.required],
      subCategory:['', Validators.required],
      type:['', Validators.required],
      color:['', Validators.required]
    });
  }

  getProduct(id) {
    this.subcatService.getById(id)
      .subscribe((response: any) => {
        // console.log(response);
        //  this.apiService.getAllUsers();
        this.editForm.setValue({
          name:response.name,
          image:response.image,
          category:response.category,
          subCategory: response.subCategory,
          type: response.type,
          color: response.color
        });
      }),
      error => {
        alert(error);
      }
  }
  ngOnInit() {
   this.getAllCategories();
  }

  getAllCategories(){
    this.catService.getAllCat().subscribe(res =>{
      this.category = res;
      // console.log(this.category);
    });
  }

  getAllSubCategories(category_id){
    this.subcatService.getAllSubCat(category_id).subscribe(res =>{
        this.subCategory = res;
        // console.log(this.subCategory);
    }),
    error => {
      alert(error);
    }
}



onFileChanged(event) {
  this.selectedFile = event.target.files[0]
}

  get f() { return this.editForm.controls; }
  public hasError = (controlName: string, errorName: string) => {
    return this.editForm.controls[controlName].hasError(errorName);
  }
  updateUser() {
    this.submitted = true;
    
    const dataToSend = new FormData();
    dataToSend.append('product_id',this.productID),
    dataToSend.append('name', this.editForm.value.name),
    dataToSend.append('image',this.selectedFile),
    dataToSend.append('category', this.editForm.value.category),
    dataToSend.append('subCategory',this.editForm.value.subCategory),
    dataToSend.append('type', this.editForm.value.type),
    dataToSend.append('color', this.editForm.value.color)


        if (this.editForm.valid) {
      this.subcatService.update(dataToSend).subscribe(
        data => {
          this.toast.success(data['message']);
          this.router.navigate(['/admin/sub']);
        },
        error => {
          this.toast.error(error['error']['message']);
          console.log(error);
        }
      );
    }

  }
}