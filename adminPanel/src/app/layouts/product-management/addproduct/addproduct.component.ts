import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ProductService } from '../../../services/product.service';
import { CategoryService} from '../../../services/category.service';
import { SubCategoryService} from '../../../services/sub-category.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css'],
  providers:[ProductService, CategoryService, SubCategoryService]
})
export class AddproductComponent implements OnInit {
  productForm: FormGroup;
  submitted = false;
  category:[];
  subCategory:[];
  fileData = null;
  selectedFile: File
  
  constructor(private route: ActivatedRoute,
    private router: Router, 
    private formBuilder: FormBuilder, 
    private http: HttpClient,
    private productService: ProductService,
    private categoryService:CategoryService,
    private subcategoryService:SubCategoryService) {
      
     }


  ngOnInit() {
    this.productForm = new FormGroup({
      product_id: new FormControl(),
      name: new FormControl(),
      category: new FormControl(),
      subCategory: new FormControl(),
      type: new FormControl(),
      color: new FormControl()
   });
   this.getAllCategories()
  }

    getAllCategories(){
      this.categoryService.getAllCat().subscribe(res =>{
        this.category = res;
        console.log(this.category);
      });
    }

    getAllSubCategories(category_id){
      this.subcategoryService.getAllSubCat(category_id).subscribe(res =>{
          this.subCategory = res;
          console.log(this.subCategory);
      }),
      error => {
        alert(error);
      }
  }
  


  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
  }

  addProduct() {
    this.submitted = true;
      const dataToSend = new FormData();
      dataToSend.append('product_id',this.productForm.value.product_id),
      dataToSend.append('name', this.productForm.value.name),
      dataToSend.append('image',this.selectedFile),
      dataToSend.append('category', this.productForm.value.category),
      dataToSend.append('subCategory',this.productForm.value.subCategory),
      dataToSend.append('type', this.productForm.value.type),
      dataToSend.append('color', this.productForm.value.color);
  

  this.productService.addProduct(dataToSend).subscribe((response)=>{
        console.log('repsonse ',response);
        this.router.navigate(["/admin/product"]);
      });
      
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.productForm.value, null, 4));
}

onReset() {
    this.submitted = false;
    this.productForm.reset();
}
}
