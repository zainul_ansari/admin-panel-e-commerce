import {Component, OnInit, ViewChild} from '@angular/core';
import { ProductService } from '../../services/product.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { DialogService } from './viewproduct/dialog.service';
import { Product } from '../../models/product'; 

const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
@Component({
  selector: 'app-product-management',
  templateUrl: './product-management.component.html',
  styleUrls: ['./product-management.component.css'],
  providers: [
    ProductService,
    DialogService
  ]
})
export class ProductManagementComponent implements OnInit {

  userData;
  displayedColumns: string[] = [
    'product_id', 
    'name', 
    'image', 
    'category',
    'category_name',
    'subCategory',
    'type',
    'color',
    'upload_date',
    'action'];
  dataSource= new  MatTableDataSource
  color= COLORS[Math.round(Math.random() * (COLORS.length - 1))];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private productService: ProductService,
     private toastr: ToastrService,
     private DialogService: DialogService) {
    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(users);
  }
  // constructor(
  //   private apiService: ApiService, 
  //   private toastr: ToastrService,
  //   ) {
  //   }


  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.listing()
  }

  listing() {
    this.productService.getAllProduct()
      .subscribe((response: any) => {
        this.userData = response
        console.log(this.userData);
        this.dataSource = new MatTableDataSource(this.userData)
      },
        error => {
          alert(error);
        })
  }
  delete(user) {
    if (confirm('Are you sure to delete this record ?') == true) {
        let index = this.userData.indexOf(user);
      this.userData.splice(index, 1);
      this.dataSource = new MatTableDataSource(this.userData)
      
      // this.selectedRemoveUser = user;
      this.productService.delete(user.product_id)
        .subscribe(x => {
          this.productService.getAllProduct();
          this.toastr.warning("Deleted Successfully", "Registered user");
        }, error => {
          alert(error);
        });
      }
  }
  openDialog(id){
    this.DialogService.selectViewUser(id);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
}