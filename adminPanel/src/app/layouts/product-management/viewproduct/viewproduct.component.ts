import { Component ,Inject} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product} from '../../../models/product';
// export interface DialogData {
//   product_id: string;
//   name: string;
  
// }

@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.css']
})
export class ViewproductComponent {

  constructor(public dialogRef: MatDialogRef<ViewproductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product) { }


  onNoClick(): void {
    this.dialogRef.close();
  }


}
