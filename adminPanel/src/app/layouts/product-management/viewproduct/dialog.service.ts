import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductService } from '../../../services/product.service';
import { ToastrService } from 'ngx-toastr';
import {ViewproductComponent} from '../viewproduct/viewproduct.component';
import { Product } from '../../../models/product';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  id;
  username;
  email;
  phone;
  password;
  constructor(public dialog: MatDialog,
    private productService: ProductService,
    private toastr: ToastrService) { }

  selectViewUser(id): void {
    this.productService.getById(id)
      .subscribe((response: any) => {
        const dialogRef = this.dialog.open(ViewproductComponent, {
          width: '100%',
          data: {
            product_id:response.product_id,
            name:response.name,
            image:response.image,
            category:response.category,
            subCategory:response.subCategory,
            type:response.type,
            color:response.color
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          this.toastr.warning("Dialog Closed", result);
        });
      }),
      error => {
        alert(error);
      }
  }

}